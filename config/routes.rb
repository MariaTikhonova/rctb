Rails.application.routes.draw do
  root to: "countries#index"
  resources :countries, only: [:index] do
    collection do
      get "search"
    end
  end
end

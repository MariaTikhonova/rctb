class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
      t.string :name
      t.decimal :population
      t.string :timezone
      t.string :phone_code
      t.string :currency_code
    end
  end
end

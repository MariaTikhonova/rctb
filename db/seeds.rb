Country.create([
  { name: "Turkmenistan", population: 25000000, phone_code: "+92", currency_code: "TM", timezone: "South Africa" },
  { name: "Albania", population: 24000000, phone_code: "+347", currency_code: "AD", timezone: "Eastern Europe"},
  { name: "Moldova", population: 17482288, phone_code: "+19", currency_code: "MD", timezone: "Eastern Europe"}
])

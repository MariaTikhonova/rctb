FactoryBot.define do
  factory :country do
    name { "USA" }
    currency_code { "USD" }
    phone_code { "+1" }
  end
end
require 'rails_helper'
RSpec.describe CountryLookup, type: :query do
  
    let!(:query) { described_class.new(record: Country.all) }
    let!(:countries) { query.call(record: Country.all, args: args) }

    describe '#call' do
    context 'search by name' do
      let(:query) { described_class.new(record: Country.all) }
      let!(:countries) { query.call(args: args) }
      let(:args) { { name: 'Poland' } }
      let!(:c)  { double(name: 'Poland') }
      let!(:c1)  { double(name: 'Romania') }
      let!(:c2)  { double(name: 'Sweden') }
   
      it 'returns countries by name' do
        expect(countries.to_a).to match_array([c])
      end
    end

    context 'search by phone_code' do
      let(:query) { described_class.new(record: Country.all, args: args) }
      let(:args) { { phone_code: '+12' } }
      let!(:c)  { double(phone_code: '+1') }
      let!(:c1)  { double(phone_code: '+38') }
      let!(:c2)  { double(phone_code: '+12') }
  
      it 'returns countries by phone_code' do
        expect(countries.to_a).to match_array([c2])
      end
    end
    
    context 'search by timezone' do
      let(:query) { described_class.new(record: Country.all, args: args) }
      let(:args) { { timezone: "Pacific/Port_Moresby" } }
      let!(:c)  { double(timezone: "Pacific/Apia") }
      let!(:c1)  { double(timezone: "Pacific/Port_Moresby") }
      let!(:c2)  { double(timezone: "Asia/Yakutsk") }
      let!(:c3)  { double(timezone: "Pacific/Port_Moresby") }

      it 'returns countries by timezone' do
        expect(countries).to match_array([c1, c3])
      end
    end
  end
end
  @Countries = React.createClass
   getInitialState: ->
    countries: @props.data
    getDefaultProps: ->
    countries: []
    paginate: true
    render: ->
      React.DOM.div
        className: 'countries'
        React.DOM.h2
          className: 'countries'
          'Countries'
        React.DOM.table
          className: 'table table-bordered'
          React.DOM.thead null,
            React.DOM.tr null,
              React.DOM.th null, 'Name'
              React.DOM.th null, 'Phone Code'
              React.DOM.th null, 'Population'
              React.DOM.th null, 'Currency Code'
              React.DOM.th null, 'Timezone'
          React.DOM.tbody null,
            for country in @state.countries
              React.createElement Country, key: country.id, country: country
class CountryLookup
  attr_accessor :record
  def initialize(record = Country.all)
    @record = record
  end
  
  def call(args)
    record ||= find_by_attrs(@record, args)
    record
  end
  
  def find_by_attrs(record, args)
    if args[:name]
      record.where("name = ?", args[:name])
    elsif args[:timezone]
      record.where("timezone = ?", args[:timezone])
    elsif args[:phone_code]
      record.where("phone_code = ?", args[:phone_code])
    end
  end
end
  
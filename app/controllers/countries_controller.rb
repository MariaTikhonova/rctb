class CountriesController < ApplicationController
  def index
    @countries = Country.all.order("name ASC").paginate(:page => params[:page], :per_page => 20)
    if params[:select2]
      @countries_for_select2 = CountryLookup.new.call(country_params)
      render 'search'
    end
  end

  def search
    @countries_for_select2 = CountryLookup.new.call(country_params)
    render @countries_for_select
  end

  private

  def country_params
    params.permit(:name, :phone_code, :timezone, :currency_code)
  end
end